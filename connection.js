(function () {

    const mysql = require('mysql');
    
    
    let config = {
        "test": {
            "host": "127.0.0.1",
            "user": "root",
            "password": "",
            "database": "users"
        }
    }
    
    const con = mysql.createConnection(config.test);
    con.connect(function (err, data) {
        if (err) {
            console.log("sql connection error :" + err.message);
        } else {
            console.log("SQL connection made sucessfully");
        }
    });
    module.exports = con;
})();