(function () {
    const express = require('express');
    const bodyParser = require('body-parser');
    const cors = require('cors');
    const cookieParser = require('cookie-parser');
    const jwt = require('jsonwebtoken');
    const connection = require('./connection');
   

    //console.log(generateSignature);

    const app = express();

    app.use(cookieParser());
    app.use(cors('combined'));
    app.use(bodyParser.json({
        limit: '50mb'
    }));
    app.use(bodyParser.urlencoded({
        extended: false
    }));

    app.post('/login', function(req,res){
        try{
            if(Object.getOwnPropertyNames(req.body).length){
                let user = req.body;
                if(user.email && user.password){
                    let query = 'select * from users where email= ?';
                    connection.query(query,[user.email], function (err, data) {
                        console.log(err,data)
                        if(!data.length){
                            res.status(500).send({message:'Invalid email'})   
                        } else {
                            if(data[0].password === user.password){
                                jwt.sign(user,'secretkey',function(err,token){
                                    if(err){
                                        res.status(500).send({message:'Internal server Error'})
                                    } else {
                                        res.cookie('auth', token, {
                                            path: '/',
                                            httpOnly: true
                                        });
                                        res.send(token);
                                    }
                                })
                            } else {
                                res.status(500).send({message:'Invalid password'})   
                            }
                        }
                    });
                } else {
                    res.status(500).send({message:'Empty email or password'})
                }
            } else {
                res.status(500).send({message:'Empty body'})
            }
        } catch (err){
            res.status(500).send(err);
        }
    })

    app.get('/',function(req,res){
        console.log(req.headers)
    })
  
    app.listen(3000,function(err,data){
        if(err)
            console.log('Error in starting server')
        else    
            console.log('App started at 3000');
    });
})();